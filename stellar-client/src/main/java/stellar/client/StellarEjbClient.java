package stellar.client;

import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarServiceRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class StellarEjbClient {

    public static void main(String[] args) throws NamingException {
        System.out.println("let's add new planet");

        String name = "stellar/stellar-service-1.0/StellarServiceImpl!stellar.service.api.StellarServiceRemote";

        Properties props = new Properties();
        props.setProperty(
                Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.naming.remote.client.InitialContextFactory");
        props.setProperty(Context.PROVIDER_URL, "http-remoting://localhost:8080");
        props.setProperty("jboss.naming.client.ejb.context", "true");

        Context context = new InitialContext(props);


        StellarServiceRemote service = (StellarServiceRemote)context.lookup(name);

        PlanetarySystem system = new PlanetarySystem();
        system.setName("ASDF");
        system.setStar("Solaris");
        system.setDistance(123.4F);
        system.setDiscovery(new Date());
        Planet p = new Planet();
        p.setName("X");
        p = service.addPlanet(p, system);

        List<Planet> planets = service.getPlanets(p.getSystem());
        planets.forEach(pl-> System.out.println(pl));
    }
}
