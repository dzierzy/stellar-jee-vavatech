package stellar.client;

import stellar.entities.PlanetarySystem;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.function.Consumer;

public class StellarRestClient {

    public static void main(String[] args) {
        System.out.println("Let's fetch planetary systems");

        PlanetarySystem system =
        ClientBuilder.newClient()
                .target("http://localhost:8080/stellar/webapi/systems/1")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(PlanetarySystem.class);

        System.out.println(system);

        List<PlanetarySystem> systems =
        ClientBuilder.newClient()
                        .target("http://localhost:8080/stellar/webapi/systems")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get(new GenericType<List<PlanetarySystem>>(){});


        systems.forEach( s -> System.out.println(s));
    }

}
