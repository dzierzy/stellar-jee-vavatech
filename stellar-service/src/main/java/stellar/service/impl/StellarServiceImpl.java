package stellar.service.impl;

import stellar.dao.Orm;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.dao.impl.inmemory.InMemoryPlanetDAO;
import stellar.dao.impl.inmemory.InMemorySystemDAO;
import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;
import stellar.service.api.StellarServiceRemote;

import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.*;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.jms.*;
import javax.transaction.*;
import java.util.List;
import java.util.logging.Logger;

@Stateless
@Local(StellarService.class)
@Remote(StellarServiceRemote.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@DeclareRoles("VIP")
@PermitAll
public class StellarServiceImpl implements StellarService, StellarServiceRemote {


    Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());


    @Inject @Orm
    private SystemDAO systemDAO;

    @Inject @Orm
    private PlanetDAO planetDAO;

    @Resource(name = "jms/CF")
    ConnectionFactory cf;

    @Resource(name = "jms/PlanetsQueue")
    Queue queue;



    @Override
    @RolesAllowed({"VIP"})
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    @PermitAll
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    @PermitAll
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        logger.info("found: " + system);
        return  system;
    }

    @Override
    @PermitAll
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }

    @Override
    public Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize) {
        return planetDAO.getPlanetsPage(s, pageNo, pageSize);
    }

    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    @TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
    public Planet addPlanet(Planet p, PlanetarySystem s) {


            systemDAO.addPlanetarySystem(s);
            p.setSystem(s);
            p = planetDAO.addPlanet(p);
            boolean fail = false;
            if(fail) {
                throw new IllegalArgumentException("another fake exception");
            }
            return p;


       /* try {
            Session session = cf.createConnection().createSession();
            p.setSystem(s);
            ObjectMessage o = session.createObjectMessage(p);
            session.createProducer(queue).send(o);

        }catch (JMSException e){
            e.printStackTrace();
        }*/

    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {
        return systemDAO.addPlanetarySystem(s);
    }



    public void setPlanetDAO(PlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    public void setSystemDAO(SystemDAO systemDAO) {
        this.systemDAO = systemDAO;
    }
}
