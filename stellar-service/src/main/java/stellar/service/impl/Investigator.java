package stellar.service.impl;

import stellar.entities.Planet;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

@MessageDriven(
        name = "Investigator",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "messagingType", propertyValue = "MessageListener"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")/*,
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/PlanetsQueue")*/
        }
)
public class Investigator implements MessageListener {

    Logger logger = Logger.getLogger(Investigator.class.getName());

    @Override
    public void onMessage(Message message) {
        try {
            Planet p = message.getBody(Planet.class);
            logger.info("about to investigate newly discovered planet " + p);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
