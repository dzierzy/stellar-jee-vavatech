<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Planetary Systems</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
</head>
<body>

<header>
    <nav>
        <ul>
            <li class="active"><a
                    href="./systems">Stellar Catalogue</a></li>
        </ul>
        <span>${pageContext.request.userPrincipal.name}</span>
    </nav>
</header>

<article>
    <jsp:include page="search.jsp"/>
    <section class="data">
