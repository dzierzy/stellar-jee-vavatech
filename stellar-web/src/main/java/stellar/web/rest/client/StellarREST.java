package stellar.web.rest.client;

import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@Path("/systems")
public class StellarREST {

    Logger logger = Logger.getLogger(StellarREST.class.getName());

    @Inject
    private StellarService service;

    @GET
    public List<PlanetarySystem> getSystems(
            @QueryParam("phrase") String phrase,
            @HeaderParam("custom-header") String customHeader,
            @CookieParam("Cookie_1") String cookie
            ){
        logger.info("about to fetch planetary systems");
        logger.info("custom-header: " + customHeader +", cookie: " + cookie);
        return phrase==null ? service.getSystems() : service.getSystemsByName(phrase);
    }

    @GET
    @Path("/{id}")
    public PlanetarySystem getSystemById(@PathParam("id") int systemId){
        logger.info("about to fetch system " + systemId);
        return service.getSystemById(systemId);
    }



    @POST
    public Response addSystem(PlanetarySystem system){
        logger.info("about to add new planetary system: " + system);
        system = service.addPlanetarySystem(system);
        //throw new IllegalArgumentException("fake exception");
        return Response.status(Response.Status.CREATED).entity(system).build();
    }



}
