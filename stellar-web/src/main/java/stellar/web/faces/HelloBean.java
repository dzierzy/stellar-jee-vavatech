package stellar.web.faces;

import javax.faces.bean.ManagedBean;

@ManagedBean(name="helloBean")
public class HelloBean {

    // Lombok
    private String name = "Brajanek";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
