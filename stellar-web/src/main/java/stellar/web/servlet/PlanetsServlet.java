package stellar.web.servlet;

import stellar.entities.Planet;
import stellar.service.api.StellarService;
import stellar.service.impl.StellarServiceImpl;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.logging.Logger;

public class PlanetsServlet extends HttpServlet {

    Logger logger = Logger.getLogger(PlanetsServlet.class.getName());

    @Inject
    StellarService service;
    // fake comment
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String systemIdString = req.getParameter("systemId");
        try {
            int systemId = Integer.parseInt(systemIdString);
            logger.info("about to fetch planets of system " + systemId);

            Principal principal = req.getUserPrincipal();
            String principalName = principal.getName();
            boolean vip = req.isUserInRole("VIP");
            logger.info("principal. name: " + principalName + ", vip:" + vip);

            List<Planet> planets = service.getPlanets( service.getSystemById(systemId) );
            req.setAttribute("planets", planets);

            req.getRequestDispatcher("/WEB-INF/jsp/planets.jsp").forward(req, resp);

        }catch (NumberFormatException e){
            throw new IllegalArgumentException("system id invalid", e);
        }
    }
}
