package stellar.web.servlet;

import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;
import stellar.service.impl.StellarServiceImpl;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class SystemsServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(SystemsServlet.class.getName());

    @Inject
    private StellarService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("about to fetch planetary systems");

        List<PlanetarySystem> systems = service.getSystems();
        forward(req, resp, systems);

    }

    private void forward(HttpServletRequest req, HttpServletResponse resp, List<PlanetarySystem> systems) throws ServletException, IOException {
        req.setAttribute("systems", systems);
        req.getRequestDispatcher("/WEB-INF/jsp/systems.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String phrase = req.getParameter("phrase");
        List<PlanetarySystem> systems = service.getSystemsByName(phrase);
        forward(req, resp, systems);
    }
}
