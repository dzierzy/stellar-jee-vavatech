package stellar.dao.impl.jdbc;

import stellar.dao.PlanetDAO;
import stellar.dao.Sql;
import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;

import javax.annotation.Resource;
import javax.enterprise.inject.Vetoed;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Sql
public class JDBCPlanetDAO implements PlanetDAO {
    @Resource(lookup = "java:jboss/datasources/StellarDS")
    private DataSource dataSource;

    @Override
    public List<Planet> getAllPlanets() {
        List<Planet> planets = new ArrayList<>();

        try(Connection conn = dataSource.getConnection(); Statement stmt = conn.createStatement();){
            ResultSet rs = stmt.executeQuery("select id, name from planet");
            while(rs.next()){
                planets.add(mapPlanet(rs));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        List<Planet> planets = new ArrayList<>();

        try(Connection conn = dataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(
                "select id, name from planet where system_id=?"
        )){
            stmt.setInt(1, system.getId());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                planets.add(mapPlanet(rs));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        Planet p = null;
        try(Connection conn = dataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(
                "select id, name from planet where id=?"
        )){
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                p = mapPlanet(rs);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return p;
    }

    @Override
    public Planet addPlanet(Planet p) {

        try(Connection conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(
                    "insert into planet (name, system_id) values (?,?)"
            )) {
            stmt.setString(1, p.getName());
            stmt.setInt(2, p.getSystem().getId());

            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()){
                p.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return p;
    }

    private Planet mapPlanet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        return new Planet(id, name, null);
    }
}
