package stellar.dao.impl.jdbc;

import stellar.dao.Sql;
import stellar.dao.SystemDAO;
import stellar.entities.PlanetarySystem;

import javax.annotation.Resource;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Vetoed;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Sql
public class JDBCSystemDAO implements SystemDAO {

    @Resource(lookup = "java:jboss/datasources/StellarDS")
    private DataSource dataSource;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {

        List<PlanetarySystem> systems = new ArrayList<>();

        try(Connection conn = dataSource.getConnection();Statement stmt = conn.createStatement();){
            ResultSet rs = stmt.executeQuery("select id, name, star, distance, discovery from planetary_system");
            while(rs.next()){
                systems.add(mapPlanetarySystem(rs));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return systems;
    }

    private PlanetarySystem mapPlanetarySystem(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String star = rs.getString("star");
        float distance = rs.getFloat("distance");
        Date discovery = rs.getDate("discovery");
        return new PlanetarySystem(id, name, star, discovery, distance);
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems = new ArrayList<>();

        try(Connection conn = dataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(
                "select id, name, star, distance, discovery from planetary_system where name like ?"
        );){
            stmt.setString(1, "%" + like + "%");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                systems.add(mapPlanetarySystem(rs));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {

        PlanetarySystem system = null;

        try(Connection conn = dataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(
                "select id, name, star, distance, discovery from planetary_system where id=?"
        );){
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                system = mapPlanetarySystem(rs);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return system;
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        try(Connection conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(
                  "insert into planetary_system (name,star,distance,discovery)values(?,?,?,?)"
            )){
            stmt.setString(1,system.getName());
            stmt.setString(2,system.getStar());
            stmt.setLong(3, (long)system.getDistance());
            stmt.setDate(4, new java.sql.Date(system.getDiscovery().getTime()));

            int rowCount = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()){
                system.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return system;
    }
}
