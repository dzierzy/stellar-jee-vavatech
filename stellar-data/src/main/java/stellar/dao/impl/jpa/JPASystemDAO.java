package stellar.dao.impl.jpa;

import stellar.dao.Orm;
import stellar.dao.SystemDAO;
import stellar.entities.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Orm
public class JPASystemDAO implements SystemDAO {

    @PersistenceContext(unitName = "stellarUnit")
    private EntityManager em;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() { // JPQL -> HQL -> SQL
        return em.createQuery("select s from PlanetarySystem s").getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return em
                .createQuery("select s from PlanetarySystem s where s.name like :name")
                .setParameter("name", "%" + like + "%")
                .getResultList();
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return em.find(PlanetarySystem.class, id);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        em.persist(system);
        return system;
    }
}
