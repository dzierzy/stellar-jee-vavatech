package stellar.dao.impl.jpa;

import stellar.dao.Orm;
import stellar.dao.PlanetDAO;
import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Orm
public class JPAPlanetDAO implements PlanetDAO {

    @PersistenceContext(unitName = "stellarUnit")
    EntityManager em;

    @Override
    public List<Planet> getAllPlanets() {
        return em.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return em
                .createQuery("select p from Planet p where p.system=:system")
                .setParameter("system", system)
                .getResultList();

    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return em.find(Planet.class, id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        em.persist(p);
        return p;
    }
}
